"Eventually this should print out stats to csv or a webpage, with tree types,
counts and pretty xml output example."

(ns talkbank-trimmer.tabulate
  (:require [clojure.xml :as xml]
            [clojure.pprint :refer [pprint]]
            [clojure.string :as str]
            [talkbank-trimmer.core :refer [parse-file sexp-tree]]))

(defn uid->path [s]
  (when s
    (let [[sid filename corpus uid] (re-matches #"((\w+?)\d+\w):(u\d+)" s)]
      [uid
       (str "/home/paul/corpora/Manchester-xml/" corpus "/" filename ".xml")])))

(first
 (let [annotations (-> "notated-examples-2.txt" slurp read-string)]
   (for [[structure {:keys [examples name]}] annotations]
     (let [example (first examples)]
       (when example
         (uid->path (:uid example)))))))

(def xd (->>
         "/home/paul/corpora/Manchester-xml/nic/nic28a.xml"
         (concat "file:")
         (apply str)
         xml/parse))

;; find utterance u343 in nic28a.xml and print it out
(->> xd
     :content
     (filter (comp (partial = "u343") :uID :attrs))
     first
     pprint)
