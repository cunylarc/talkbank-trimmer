(ns talkbank-trimmer.core
  (:require [clojure.xml :as xml]
            [clojure.pprint :refer [pprint]]
            [clojure.string :as str]))

(defn node-name
  "Returns a keyword that represents the node in the tree."
  [{:keys [tag attrs] :as node}]
  (let [node-type (:type attrs)
        node-name (as-> [(name tag) node-type] $
                    (remove nil? $)
                    (interpose "." $)
                    (apply str $)
                    (str/replace $ " " "-"))
        ;; node-name (apply str (interpose "-" (remove nil? [(name tag) node-type])))
        ]
    (keyword
     (if (= node-type "tag")
       (str tag ":"
            (-> node :content first
                :content first
                :content second
                :content first
                (str/replace " " "-")))
       node-name))))


(defn process-sexp
  "remove top-level nils and unwrap single item lists; (a (b) c nil) -> (a b c)"
  [sexp]
  (let [sexp (remove nil? sexp)]
    (if (= 1 (count sexp))
      (first sexp)
      sexp)))

(defn sexp-tree
  "takes an xml node an returns a reduced tree representation of it,
  only retaining the interesting parts of its structure."
  ([node]
   (sexp-tree {} node))
  ([metadata {:keys [tag attrs content] :as node}]
   (cond
     ;; ignore this node level, work on the deeper nodes
     (= :CHAT tag) (process-sexp (map (partial sexp-tree metadata) content))
     (= :u tag) (let [metadata (assoc metadata :uid (:uID attrs))]
                  (process-sexp (map (partial sexp-tree metadata) content)))

     (string? node) 'text

     ;; ignore these nodes
     (= :gra tag) nil
     (= :Participants tag) nil
     (re-matches #"^comment.*" (name tag)) nil

     ;; include this node on the tree, recurse on its children
     :else (let [item (process-sexp (cons (node-name node)
                                          (map (partial sexp-tree metadata) content)))]
             (if (keyword? item)
               item
               (with-meta item (let [{:keys [uid path]} metadata]
                                 {:uid (str path ":" uid)})))))))

(defn counter
  "Returns a frequency count map {item: count} of the items in `coll`.
  If a frequency count map is passed as a second argument, use it as
  the starting map, incrementing it."
  ([coll]
   (counter coll {}))
  ([coll counts]
   (reduce (fn [counts item]
             (as-> counts $
               (update-in $ [item :count] (fnil inc 0))
               (update-in $ [item :examples] (fn [m]
                                               (shuffle
                                                (take 5 (conj
                                                         (into #{} m)
                                                         (meta item))))))))
           counts
           coll)))

(defn node->tree-counts
  "takes a talkbank xml node and a frequency count map. returns the
  frequency count map updated with the tree counts observed in the xml
  node."
  ([node]
   (node->tree-counts node {}))
  ([node counts]
   (counter
    (reduce #(concat %1 %2) node)
    counts)))

(defn parse-file
  "accepts an xml filename as a path and returns a 'reduced' tree
  structure representation of it."
  [path]
  (->> path
       (concat "file:")
       (apply str)
       xml/parse
       (sexp-tree {:path (-> path (str/split #"/") last (str/split #"\.") first)})))

(defn swap [[a b]] [b a])

(defn -main
  "Reads input xml filenames and prints out a frequency count of
  all (reduced) tree structures encountered in the xml files."
  [& fns]
  (let [tree-counts (reduce ; merge tree counts together from all files
                     (fn [counts path] (node->tree-counts (parse-file path) counts))
                     {}
                     fns)]
    (->> tree-counts
         (sort-by (comp :count second))
         reverse
         pprint))

  #_(doseq [file fns]
      (let [output (with-out-str (->> file
                                      (concat "file:")
                                      (apply str)
                                      xml/parse
                                      ;; (trim-tree 0)
                                      ;; xml/emit
                                      sexp-tree
                                      clojure.pprint/pprint))]
        (println output))))


(def trees (-> "with-meta-and-text.txt" slurp read-string))
(let [counts (map (comp :count last) trees)
      total (reduce + counts)
      mass (reductions + counts)
      threshold 0.99]
  {:cases-required (->> mass
                        (take-while #(< (/ % total) threshold))
                        count)
   :utterances-missed (* total (- 1 threshold))})
(->> trees (take 37) pprint)

(let [notes (-> "notated-examples-2.txt" slurp read-string)]
  (->> notes
       (filter (comp :note second))
       pprint))
