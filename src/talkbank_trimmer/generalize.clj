(ns talkbank-trimmer.generalize
  (:require [clojure.xml :as xml]
            [clojure.pprint :refer [pprint]]
            [clojure.string :as str]
            [talkbank-trimmer.core :refer [parse-file sexp-tree]]))

(defn simple-counter
  "Returns a frequency count map {item: count} of the items in `coll`.
  If a frequency count map is passed as a second argument, use it as
  the starting map, incrementing it."

  [coll]
  (reduce (fn [counts item]
            (update counts item (fnil inc 0)))
          {}
          coll))


(defn read-annotations
  [filename]
  (for [[tree notes] (-> filename slurp read-string)]
    {:tree tree
     :notes notes}))

(defn shallow-tree
  ([tree]
   (shallow-tree 1 tree))
  ([depth tree]
   (cond (and (coll? tree) (= depth 0)) (first tree)
         (coll? tree) (map #(shallow-tree (dec depth) %) tree)
         :else tree)))

(defn drop-dupes
  [coll]
  (loop [[x & xs] coll
         prev nil
         acc []]
    (cond
      (nil? x) acc
      (= prev x) (recur xs x acc)
      :else (recur xs x (conj acc x)))))

(defn node=
  "returns true if the first keyword in node is node-type"
  [node-type tree]
  (if (coll? tree)
    (= node-type (first tree))
    (= node-type tree)))

(defn find-node
  ([pred node]
   (let [found (atom [])]
     (find-node pred node found)
     @found))
  ([pred node found]
   (cond
     (pred node) (swap! found conj node)

     (coll? node) (doseq [x node]
                    (find-node pred x found)))))
(defn find-node=
  [node-type node]
  (find-node (partial node= node-type) node))

(defn find-node-child=
  [node-type node]
  (find-node
   (fn [target] (when (coll? target)
                  (some (partial node= node-type) (rest target))))
   node))

(defn lmap [fun]
  (fn [x]
    (if (coll? x) (fun x) x)))

(defn find-example-pred
  ([pred]
   (find-example-pred pred 1))
  ([pred depth]
   (->> "with-meta-and-text.txt"
        read-annotations
        (map (comp pred :tree))
        (reduce concat)
        (map (lmap (comp drop-dupes (partial shallow-tree depth))))
        simple-counter
        (sort-by second))))

(defn find-examples
  ([node-type]
   (find-examples node-type 1))
  ([node-type depth]
   (find-example-pred (partial find-node= node-type) depth)))

(defn find-context
  ([node-type]
   (find-context node-type 1))
  ([node-type depth]
   (find-example-pred (partial find-node-child= node-type) depth)))

(->> "with-meta-and-text.txt"
     read-annotations
     (map (comp (lmap first) :tree))
     simple-counter
     (sort-by second)
     pprint)
