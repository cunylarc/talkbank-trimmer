# talkbank-trimmer

This repo exists to identify some of the tree-shaped patterns within the
talkbank CHILDES files. See notated-examples-2.txt for a list of subtrees,
identified along with counts and descriptions of what they represent.